CREATE TABLE Categories (
    name char(255),
    description char(255)
);

CREATE TABLE Topics (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    author char(255),
    title char(255),
    date datetime,
    category char(255)
);

CREATE TABLE Replies (
    author char(255),
    content text(1000000),
    date datetime,
    topic INTEGER
);

INSERT INTO Categories (name,description) VALUES ('Announcements', 'Important stuff'), ('Introductions', 'Introduce yourself here');
