#!/bin/python3
import jinja2, cgi, sqlite3, os, datetime

db = sqlite3.connect('data.db')
loader = jinja2.FileSystemLoader(searchpath='./')
env = jinja2.Environment(loader=loader)
r = cgi.FieldStorage()
username = os.environ['REMOTE_USER']
date = datetime.datetime.now()

def build_category(category):
    topics = db.execute('SELECT * FROM Topics WHERE category=?', (category,)).fetchall()
    template = env.get_template('category.html')
    html = template.render(topics=topics, category=category)
    open(f'public/cat_{category}.html', 'w').write(html)

def build_categories():
    categories = db.execute('SELECT * FROM Categories').fetchall()
    template = env.get_template('index.html')
    html = template.render(categories=categories)
    open('public/index.html', 'w').write(html)

    for category in categories:
        build_category(category[0])

def build_topic(topic_id):
    topic = db.execute('SELECT * FROM Topics WHERE id=?', (topic_id,)).fetchone()
    replies = db.execute('SELECT * FROM Replies WHERE topic=?', (topic_id,)).fetchall()
    template = env.get_template('topic.html')
    html = template.render(topic=topic, replies=replies)
    open(f'public/topic_{topic_id}.html', 'w').write(html)

if r.getvalue('submit_topic'):
    title = r.getvalue('title')
    category = r.getvalue('submit_topic')
    if title == None:
        title = "No title provided"
    db.execute('INSERT INTO Topics (author,title,date,category) VALUES (?,?,?,?)', (username,title,date,category,))
    db.commit()
    topic_id = db.execute('SELECT max(id) FROM Topics').fetchone()[0]
    build_topic(topic_id)
    build_category(category)
    final = f'<meta http-equiv="refresh" content="0;url=\'/topic_{topic_id}.html\'" />'

elif r.getvalue('submit_reply'):
    content = r.getvalue('content')
    topic_id = r.getvalue('submit_reply')
    if content == None:
        content = "No content provided"
    db.execute('INSERT INTO Replies (author,content,date,topic) VALUES (?,?,?,?)', (username,content,date,topic_id,))
    db.commit()
    build_topic(topic_id)
    final = f'<meta http-equiv="refresh" content="0;url=\'/topic_{topic_id}.html\'" />'

else:
    final = f'<meta http-equiv="refresh" content="0;url=\'/index.html\'" />'
    final = r.getvalue('submit_topic')

print('Content-type: text/html\n')
print(final)
